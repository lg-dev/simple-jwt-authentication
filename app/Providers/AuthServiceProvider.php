<?php

namespace App\Providers;

use App\Authentication\ClaimsFactory;
use App\Authentication\Factory;
use App\Authentication\JwtAuth;
use App\Authentication\Parser;
use App\Authentication\Providers\Authentication\EloquentProvider;
use App\Authentication\Providers\Jwt\FirebaseProvider;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{

    protected $provides = [
        JwtAuth::class
    ];

    public function boot(ResponseFactory $response) {

        $this->app->bind(JwtAuth::class, function() {
            $authProvider = new EloquentProvider();

            $claimsFactory = new ClaimsFactory(
                $this->app->get('request')
            );

            $jwtProvider = new FirebaseProvider();

            $factory = new Factory($claimsFactory, $jwtProvider);

            $parser = new Parser($jwtProvider);

            return new JwtAuth($authProvider, $factory, $parser);
        });
    }
}
