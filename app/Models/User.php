<?php

namespace App\Models;

use App\Authentication\Contracts\JwtSubject;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements JwtSubject
{

    protected $fillable = [
        'username','name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJwtIdentifier() {
    	return $this->id;
    }
}
