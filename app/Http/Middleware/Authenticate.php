<?php

namespace App\Http\Middleware;

use App\Authentication\JwtAuth;
use Closure;
use Http\Client\Exception;
use Illuminate\Http\Request;
// use Illuminate\Http\Response;

class Authenticate
{

    protected $auth;

    public function __construct(JwtAuth $auth) {
        $this->auth = $auth;
    }

    public function __invoke(Request $request, Closure $next) {

        if(!$header = $this->getAuthorizationHeader($request)) {
            return abort(401);
        }

        try {
            $a = $this->auth->authenticate($header);
        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 401);

            // Normall abort(401);
        }

        return $next($request);
    }

    protected function getAuthorizationHeader(Request $request) {
        
        $authorization = array_values($request->header());

        if(!list($header) = $authorization[5]) {
            return false;
        }

        return $header;
    }
}
