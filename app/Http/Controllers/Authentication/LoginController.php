<?php

namespace App\Http\Controllers\Authentication;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Authentication\JwtAuth;

class LoginController extends Controller {

    protected $auth;

    public function __construct(JwtAuth $auth) {
        $this->auth = $auth;
    }
    
    public function index(Response $response , Request $request) {

      if(!$token = $this->auth->attempt($request->get('email'), $request->get('password'))) {
        return abort(401);
      }

      return response()->json([
        'token' => $token,
      ]);
    }
}
