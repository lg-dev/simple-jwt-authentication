<?php

namespace App\Http\Controllers\Authentication;

use App\Http\Controllers\Controller;
use App\Http\Requests\Authentication\RegisterFormRequest;
use App\Models\User;
use Http\Client\Exception;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index(RegisterFormRequest $request, User $user) {

       $attempt = $user->create([
            'username' => $request->json('username'),
            'name'     => $request->json('name'),
            'email'    => $request->json('email'),
            'password' => bcrypt($request->json('password')),
        ]);

        try { 

            // ideally we want to return them to some page and inject the response through
           $response = $attempt->id;

           return response()->json($response);
         
        } catch(Exception $e) {
            return response()->json('Bad Request', 400);
        }

        // return $user;
    }
}
