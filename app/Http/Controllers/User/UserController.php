<?php

namespace App\Http\Controllers\User;

use App\Authentication\JwtAuth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    protected $auth;

    public function __construct(JwtAuth $auth) {
       $this->auth = $auth;
    }

    public function index(Request $request, Response $response) {
        return response()->json($this->auth->user());
    }
}
