<?php

namespace App\Authentication;

use App\Authentication\Contracts\JwtSubject;
use App\Authentication\Factory;
use App\Authentication\Parser;
use App\Authentication\Providers\Authentication\AuthServiceProvider;

class JwtAuth {

   protected $auth;
   protected $factory;
   protected $parser;
   protected $user = null;

   public function __construct(AuthServiceProvider $auth, Factory $factory, Parser $parser) {
     $this->auth = $auth;
     $this->factory = $factory;
     $this->parser = $parser;
   }
    
   public function attempt($email, $password) {

      if(!$user = $this->auth->byCredentials($email, $password)) {
        return false;
      }

      return $this->fromSubject($user);
    }

    public function authenticate($token) {
      global $user;

      $user = $this->auth->byId(
        $this->parser->decode($token)->sub
      );

      return $this;
    }

    public function user() {
      global $user;
      return $user;
    }

    // Chaining this method here start to build the subject for the payload
    protected function fromSubject(JwtSubject $subject) {
      return $this->factory->encode($this->makePayload($subject));
    }

    // This makes the payload
    protected function makePayload(JwtSubject $subject) {
      return $this->factory->withClaims($this->getClaimsForSubject($subject))->make();
    }

    protected function getClaimsForSubject(JwtSubject $subject) {
      // Gets the Claims for our subject
      return [
        'sub' => $subject->getJwtIdentifier()
      ];
    }


}