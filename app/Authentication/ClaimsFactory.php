<?php

namespace App\Authentication;

use Carbon\Carbon;
// use Psr\Http\Message\RequestInterface;
use Illuminate\Http\Request;

class ClaimsFactory 
{
    protected $defaultClaims = [
        'iss',
        'iat',
        'exp',
        'nbf',
        'jti',
    ];

    protected $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function getDefaltClaims() {
        return $this->defaultClaims;
    }

    public function iss() {
        return $this->request->path();
    }

    public function iat() {
        return Carbon::now()->getTimestamp();
    }

    public function nbf() {
        return Carbon::now()->getTimestamp(); 
    }

    public function jti() {
        return bin2hex(str_random(32));
    }

    public function exp() {
        return Carbon::now()->addMinutes($this->getExp())->getTimestamp();
    }

    public function get($claim) {
        
        if(!method_exists($this, $claim)) {
            return null;
        }

        return $this->{$claim}();
    }

    public function getExp() {
        return config('app.jwt.expiry');
    }
}