<?php

namespace App\Authentication\Providers\Authentication;

use App\Authentication\Providers\Authentication\AuthServiceProvider;
use App\Models\User;

class EloquentProvider implements AuthServiceProvider {
    
    public function byCredentials($email, $password) {
     
     if(!$user = User::where('email', $email)->first()) {
        return null;
      }

      if(!password_verify($password, $user->password)) {
        return null;
      }

      return $user;
    }

    public function byId($id) {
        return User::where('id','=',$id)->first();
    }
}