<?php 

namespace App\Authentication\Providers\Jwt;

use App\Authentication\Providers\Jwt\JwtProviderInterface;
use Firebase\JWT\JWT;

class FirebaseProvider implements JwtProviderInterface 
{
    public function encode(array $claims) {
        return JWT::encode($claims, $this->getSecret(), $this->getAlgorithm());
    }

    public function decode($token) {
        return JWT::decode($token, $this->getSecret(), [$this->getAlgorithm()]);
    }

    public function getSecret() {
        return config('app.jwt.secret');
    }

    public function getAlgorithm() {
        return config('app.jwt.algo');
    }
}